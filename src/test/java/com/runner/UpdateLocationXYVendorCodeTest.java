package com.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(features = {
		"src/test/resources/features/update_location_xy_vendorcode.feature" }, monochrome = true, glue = {
				"com.steps.validvendor" })

public class UpdateLocationXYVendorCodeTest extends AbstractTestNGCucumberTests {

}
