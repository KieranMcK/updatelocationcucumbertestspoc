package com.steps.validvendor;

import static io.restassured.RestAssured.given;
import static org.testng.Assert.assertEquals;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;
import resource.PayLoad;
import resource.Resource;



public class Update_Location_XY_VendorCode {

	private final static String VALID_VENDOR_CODE_XY_RESPONSE = "{\"locationcontentresponse\":{\"status\":\"ACCEPTED\",\"message\":\"Location Content accepted. Import in progress\",\"debugMessage\":null,\"subErrors\":null}}";

	Properties pr = new Properties();
	String res;

	private RequestSpecification request;

	@Before
	public void readProperties() throws IOException {
		FileInputStream fi = new FileInputStream("src/test/resources/env.properties");
		pr.load(fi);
		System.out.println("\nAPI-Cucumber-Test 3");
		System.out.println("*** TEST VALID_VENDOR_CODE_XY *** ");
	}

	@Given("^I am using the QA API$")
	public void i_am_using_the_QA_API() throws Throwable {

		RestAssured.baseURI = pr.getProperty("HOST");
	}

	@Given("^the vendor code (.*)$")
	public void the_vendor_code_XY(String vendorCode) throws Throwable {
		String newRequestString = PayLoad.UPDATE_LOCATION_CODE().replaceAll("VENDOR_CODE_TEST_VARIABLE", vendorCode);
		request = given().body(newRequestString);
	}

	@When("^I update DUB with the following details$")
	public void i_update_DUB_with_the_following_details() throws Throwable {
		String response = request.given().header("Content-Type", "application/json").when()
				.post(Resource.UPDATE_LOCATION_RESOURCES()).asString();

		res = response;

	}

	@Then("^I should receive a message saying that the update was accepted with no errors$")
	public void i_should_receive_a_message_saying_that_the_update_was_accepted_with_no_errors() 
			throws Throwable {

		assertEquals(res, VALID_VENDOR_CODE_XY_RESPONSE);
		
		System.out.println("PASSED ");
	}

}
