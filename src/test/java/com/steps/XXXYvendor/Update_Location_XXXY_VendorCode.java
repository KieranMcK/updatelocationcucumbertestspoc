package com.steps.XXXYvendor;

import static io.restassured.RestAssured.given;
import static org.testng.Assert.assertEquals;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;
import resource.PayLoad;
import resource.Resource;

public class Update_Location_XXXY_VendorCode {
	
	private final static String INVALID_VENDOR_CODE_XXXY_RESPONSE = "{\"locationcontentresponse\":{\"status\":\"ACCEPTED\",\"message\":\"Location Content invalid. Import failed. Invalid location content needs to be rectified and re-imported. See error(s) below.\",\"debugMessage\":null,\"subErrors\":[{\"object\":\"LocationDetails[0]\",\"field\":\"code\",\"rejectedValue\":\"111111111111111111\",\"message\":\"code must contain between 1 & 10 characters\"},{\"object\":\"LocationDetails[1]\",\"field\":\"vendorCode\",\"rejectedValue\":\"XX\",\"message\":\"vendorCode provided is invalid\"}]}}";
	
	Properties pr = new Properties();
	String res;

	private RequestSpecification request;

	@Before
	public void readProperties() throws IOException {
		FileInputStream fi = new FileInputStream("src/test/resources/env.properties");
		pr.load(fi);
		System.out.println("\nAPI-Cucumber-Test 2");
		
		System.out.println("*** TEST INVALID_VENDOR_CODE_XXXY *** ");
	}

	@Given("^I am using the QA API$")
	public void i_am_using_the_QA_API() throws Throwable {
		
		RestAssured.baseURI = pr.getProperty("HOST");
	}

	@Given("^the code (\\d+)$")
	public void the_vendor_code_XXXY(int code) throws Throwable {
		request = given().body(PayLoad.UPDATE_LOCATION_CODE_XXXY());
	}

	@When("^I update DUB with the following details$")
	public void i_update_DUB_with_the_following_details() throws Throwable {
		String response = request.given().header("Content-Type", "application/json").when()
				.post(Resource.UPDATE_LOCATION_RESOURCES()).asString();

		res = response;
	}
	
	@Then("^I should receive a message saying that the update was accepted with an error$")
	public void i_should_receive_a_message_saying_that_the_update_was_accepted_with_error()
			throws Throwable {

		assertEquals(res, INVALID_VENDOR_CODE_XXXY_RESPONSE);
		
		System.out.println("PASSED ");
	}

}
