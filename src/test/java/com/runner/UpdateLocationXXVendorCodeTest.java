package com.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(features = {
		"src/test/resources/features/update_location_xx_vendorcode.feature" }, monochrome = true, glue = {
				"com.steps.invalidvendor" })

public class UpdateLocationXXVendorCodeTest extends AbstractTestNGCucumberTests {

}
