package com.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(features = {
		"src/test/resources/features/update_location_xxxy_vendorcode.feature" }, monochrome = true, glue = {
				"com.steps.XXXYvendor" })

public class UpdateLocationXXXYVendorCodeTest extends AbstractTestNGCucumberTests {

}
