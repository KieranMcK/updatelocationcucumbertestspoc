package resource;

public class PayLoad 
{
	
	public static String UPDATE_LOCATION_CODE()
	{
		String body= "{"+
				  " \"locationDetails\": [ "+
				  "{"+
				      
				  "\"id\": 0, "+
				  "  \"name\": \"Example Location Name\", "+
				  "  \"code\": \"ABCD12\", "+
				  "  \"airportCode\": \"DUB\", "+
				  "  \"vendorCode\": \"VENDOR_CODE_TEST_VARIABLE\", "+
				  "  \"atAirport\": false, "+
				  "  \"onTerminal\": false, "+
				  "  \"cityLocation\": \"C\", "+
				  "  \"active\": true, "+
				  "  \"address\": { "+
				  "  \"addressLine1\": \"Street\", "+
				  "  \"addressLine2\": \"Town\",  "+
				  "  \"addressLine3\": \"City\", "+
				  "  \"addressLine4\": \"Provence\", "+
				  "  \"postCode\": \"123456\", "+
				  "  \"countryCode\": \"IE\", "+
				  "  \"longitude\": 51.123456, "+
				  "  \"latitude\": 51.654321 "+
				  " }, "+
				  "  \"telephone\": { "+  
				  "  \"phoneNumber\": \"087123456\"}, "+
				  "  \"additionalInfo\": { "+
			      "  \"additionalDetails\": \"additional information on location\", "+
				  "  \"counterCarLocation\": \"TERM\", "+
				  "  \"directions\": \"Go to desk after arrivals\", "+
				  " \"operationSchedules\": { "+  
				  "    \"operationSchedulesList\": [ "+
				          "  { "+
				              
				          " \"day\": \"Monday\",  "+
				           "   \"start\": \"09:00\", "+
				            "  \"end\": \"17:00\" "+
				           " } "+
				           "]"+
				         "},"+
				           " \"shuttleService\": \"Shuttle service information\" "+
				      "}"+
				    "}"+
				  "]"+
				"}";
		return body;
	}
	
	public static String UPDATE_LOCATION_CODE_XXXY()
	{
			String body= "{\r\n" + 
					"  \"locationDetails\": [\r\n" + 
					"    {\r\n" + 
					"      \"id\": 0,\r\n" + 
					"      \"name\": \"Example Location Name\",\r\n" + 
					"      \"code\": \"111111111111111111\",\r\n" + 
					"      \"airportCode\": \"DUB\",\r\n" + 
					"      \"vendorCode\": \"XY\",\r\n" + 
					"      \"atAirport\": false,\r\n" + 
					"      \"onTerminal\": false,\r\n" + 
					"      \"cityLocation\": \"C\",\r\n" + 
					"      \"active\": true,\r\n" + 
					"      \"address\": {\r\n" + 
					"        \"addressLine1\": \"Street\",\r\n" + 
					"        \"addressLine2\": \"Town\",\r\n" + 
					"        \"addressLine3\": \"City\",\r\n" + 
					"        \"addressLine4\": \"Provence\",\r\n" + 
					"        \"postCode\": \"123456\",\r\n" + 
					"        \"countryCode\": \"IE\",\r\n" + 
					"        \"longitude\": 51.123456,\r\n" + 
					"        \"latitude\": 51.654321\r\n" + 
					"      },\r\n" + 
					"      \"telephone\": {\r\n" + 
					"        \"phoneNumber\": \"087123456\"\r\n" + 
					"      },\r\n" + 
					"      \"additionalInfo\": {\r\n" + 
					"        \"additionalDetails\": \"additional information on location\",\r\n" + 
					"        \"counterCarLocation\": \"TERM\",\r\n" + 
					"        \"directions\": \"Go to desk after arrivals\",\r\n" + 
					"        \"operationSchedules\": {\r\n" + 
					"          \"operationSchedulesList\": [\r\n" + 
					"            {\r\n" + 
					"              \"day\": \"Monday\",\r\n" + 
					"              \"start\": \"09:00\",\r\n" + 
					"              \"end\": \"17:00\"\r\n" + 
					"            }\r\n" + 
					"          ]\r\n" + 
					"        },\r\n" + 
					"        \"shuttleService\": \"Shuttle service information\"\r\n" + 
					"      }\r\n" + 
					"    },{\r\n" + 
					"      \"id\": 0,\r\n" + 
					"      \"name\": \"Example Location Name\",\r\n" + 
					"      \"code\": \"ABCD12\",\r\n" + 
					"      \"airportCode\": \"ORK\",\r\n" + 
					"      \"vendorCode\": \"XX\",\r\n" + 
					"      \"atAirport\": false,\r\n" + 
					"      \"onTerminal\": false,\r\n" + 
					"      \"cityLocation\": \"C\",\r\n" + 
					"      \"active\": true,\r\n" + 
					"      \"address\": {\r\n" + 
					"        \"addressLine1\": \"Street\",\r\n" + 
					"        \"addressLine2\": \"Town\",\r\n" + 
					"        \"addressLine3\": \"City\",\r\n" + 
					"        \"addressLine4\": \"Provence\",\r\n" + 
					"        \"postCode\": \"123456\",\r\n" + 
					"        \"countryCode\": \"IE\",\r\n" + 
					"        \"longitude\": 51.123456,\r\n" + 
					"        \"latitude\": 51.654321\r\n" + 
					"      },\r\n" + 
					"      \"telephone\": {\r\n" + 
					"        \"phoneNumber\": \"087123456\"\r\n" + 
					"      },\r\n" + 
					"      \"additionalInfo\": {\r\n" + 
					"        \"additionalDetails\": \"additional information on location\",\r\n" + 
					"        \"counterCarLocation\": \"TERM\",\r\n" + 
					"        \"directions\": \"Go to desk after arrivals\",\r\n" + 
					"        \"operationSchedules\": {\r\n" + 
					"          \"operationSchedulesList\": [\r\n" + 
					"            {\r\n" + 
					"              \"day\": \"Monday\",\r\n" + 
					"              \"start\": \"09:00\",\r\n" + 
					"              \"end\": \"17:00\"\r\n" + 
					"            }\r\n" + 
					"          ]\r\n" + 
					"        },\r\n" + 
					"        \"shuttleService\": \"Shuttle service information\"\r\n" + 
					"      }\r\n" + 
					"    }\r\n" + 
					"  ]\r\n" + 
					"}"; 
		
	
	
		
		return body;
	}
	
}
